import os

# This function make an .mp4 movie from a set of 00000.png files !
# /!\ Il faut générer des images aux dimensions PAIRES


def mkmovie(pngdir,fps,moviedir="",moviename="movie"):
    os.system("ffmpeg -y -r {:} -i ".format(fps)+pngdir+"%5d.png -c:v libx264 -crf 15 -profile:v baseline -level 5.0 -pix_fmt yuv420p   -c:a aac -ac 2 -b:a 128k   -movflags faststart -vf 'pad=ceil(iw/2)*2:ceil(ih/2)*2' "+moviedir+moviename+".mp4") 
