import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.collections as mcoll
import matplotlib.path as mpath


def getfiglatex(columnwidth=500.484, wf=1.0, hf=1.0/1.618,pt=10):
	# Retourne une figure de taille relative wf en largueur et wf*hf en hauteur
	# La taille relative est calculée par rapport à la largeur du document latex
	# Accessible par la commande \the\columnwidth
	
	# Pour que matplotlib puisse génerer du texte en latex de même police que le .tex
	mpl.rcParams['text.latex.preamble']=[r"\usepackage{lmodern}",r'\usepackage{amsmath}',r'\usepackage{amssymb}']
	mpl.rcParams['font.family'] = 'lmodern'
	
	# A deconmenter pour sciences advances
	# ~ mpl.rcParams["font.sans-serif"] = "Computer Modern Sans serif"
	# ~ mpl.rcParams['text.latex.preamble']=[ r'\usepackage{sansmath}',r'\sansmath']
	
	mpl.rcParams['text.usetex'] = True
	mpl.rcParams['font.size'] = pt
	
	   
	
	# Calcule les dimensions de la figure matplotlib par rapport à la largeur du .tex
	fig_width_pt = columnwidth*wf
	inches_per_pt = 1.0/72.27
	fig_width = fig_width_pt*inches_per_pt
	fig_height = fig_width*hf
	
	# Retourne la figure
	return plt.figure(figsize=[fig_width,fig_height])

def colorline(
    x, y, z=None, cmap=plt.get_cmap('copper'), norm=plt.Normalize(0.0, 1.0),
        linewidth=3, alpha=1.0):
    """
    http://nbviewer.ipython.org/github/dpsanders/matplotlib-examples/blob/master/colorline.ipynb
    http://matplotlib.org/examples/pylab_examples/multicolored_line.html
    Plot a colored line with coordinates x and y
    Optionally specify colors in the array z
    Optionally specify a colormap, a norm function and a line width
    """

    # Default colors equally spaced on [0,1]:
    if z is None:
        z = np.linspace(0.0, 1.0, len(x))

    # Special case if a single number:
    if not hasattr(z, "__iter__"):  # to check for numerical input -- this is a hack
        z = np.array([z])

    z = np.asarray(z)

    segments = make_segments(x, y)
    lc = mcoll.LineCollection(segments, array=z, cmap=cmap, norm=norm,
                              linewidth=linewidth, alpha=alpha)

    ax = plt.gca()
    ax.add_collection(lc)

    return lc


def make_segments(x, y):
    """
    Create list of line segments from x and y coordinates, in the correct format
    for LineCollection: an array of the form numlines x (points per line) x 2 (x
    and y) array
    """

    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    return segments
